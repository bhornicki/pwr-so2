#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#define UNUSED(x) (void)(x)

bool line_equals(char *line, char *string);

int main(int argc, char const *argv[])
{
    int sock = 0, read_len, addr_len;
    struct sockaddr_in serv_addr;
    char* buffer = NULL;
    size_t len = 0;
    long port;
    fd_set fd_set;
    struct timeval fd_timeout;
       
    if(argc != 3)
    {
        printf("[error] Invalid argument count! Call \"%s address port_number\"\n", argv[0]);
        exit(1);
    }
    port = strtol(argv[2], NULL, 10);
    if(port >= 65535)
    {
        printf("[error] Invalid port! Correct range is 0-65535");
        exit(1);
    }
    

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        printf("[error] socket\n");
        return -1;
    }
   
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
       
    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0) 
    {
        printf("[error] inet_pton\n");
        return -1;
    }
   
    FD_ZERO(&fd_set);

    int fd_ready;
    while (true)
    {
        FD_SET(sock, &fd_set);

        getline(&buffer, &len, stdin);

        for (size_t repeat = 0; repeat < 3; repeat++)
        {
            sendto(sock, buffer, strlen(buffer),
                0, (const struct sockaddr *) &serv_addr, sizeof(serv_addr));

            fd_timeout.tv_sec = 5;  //nice ping bro
            fd_timeout.tv_usec = 0;
            fd_ready = select(sock+1, &fd_set, NULL, NULL, &fd_timeout);
            if(fd_ready < 0)
            {
                printf("[error] select\n");
                return -1;
            }
            if(fd_ready==0) //no updates; repeat
            {
                if(repeat == 2)
                {
                    printf("[error] no response\n");
                    return -1;
                }
                printf("[info] no response; repeating send command...\n");
                continue;
            }
            addr_len = sizeof(serv_addr);
            read_len = recvfrom(sock, buffer, 1024, 0, (struct sockaddr *) &serv_addr , &addr_len);

            if(line_equals(buffer,"quit")
                || line_equals(buffer,"exit"))
            {
                printf("[info] closing connection\n");
                shutdown(sock, SHUT_WR);
                while(read_len !=0)
                {
                    addr_len = sizeof(serv_addr);
                    read_len = recvfrom(sock, buffer, 1024, 0, (struct sockaddr *) &serv_addr , &addr_len);
                }
                shutdown(sock, SHUT_RD);
                close(sock);
                exit(0);
            }

            printf("[received] %s\n",buffer );
            break;
        }
        

    }
    return 0;
}

bool line_equals(char *line, char *string)
{
    size_t len = 0;
    while(true)
    {
        char c1 = line[len];
        char c2 = string[len];
        if(c1 == '\0'
            || c1 == '\r'
            || c1 == '\n')
            return (c2  == '\0');    //true when both ended, false when string did not end yet
        if(c2  == '\0')
            return false;           //string ended first
        if(tolower(c1)!=tolower(c2))
            return false;           //content mismatch
        len++;
    }
}