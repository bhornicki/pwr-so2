#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <signal.h>

#define UNUSED(x) (void)(x)

//constexpr please
#define BUFFER_SIZE 1024
#define CLIENT_COUNT 1024
bool line_equals(char *line, char *string);
int main(int argc, char const *argv[])
{
    long port;
    int sockfd;
    struct sockaddr_in socket_server, socket_client;
    
    signal(SIGCHLD, SIG_IGN); //Silently exterminate children. 
    
    if (argc != 2)
    {
        printf("Invalid argument count! Call \"%s port_number\"\n", argv[0]);
        exit(1);
    }
    port = strtol(argv[1], NULL, 10);
    if (port > 65535)
    {
        printf("Invalid port! Correct range is 0-65535\n");
        exit(1);
    }
    
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
		printf("[error] socket");
		exit(EXIT_FAILURE);
	}
	
    memset(&socket_server, 0, sizeof(socket_server));
	memset(&socket_client, 0, sizeof(socket_client));
	
	socket_server.sin_family = AF_INET;
	socket_server.sin_addr.s_addr = INADDR_ANY;
	socket_server.sin_port = htons(port);

    if (bind(sockfd, (struct sockaddr *)&socket_server, sizeof(socket_server)) < 0)
    {
        printf("Error: bind failed\n");
        exit(1);
    }
    
    printf("Listening on port %ld\n", port);
    
    struct sockaddr_in clients[CLIENT_COUNT] = {};  //would rather use some kind of vector
                        //or std::list<std::tuple<sockaddr_in, std::stringstream>>
                        //so disconnected client removal and received line parsing would be easy
                        //for a while I thought about making C wrappers for std functions...
    int next_client_idx = 0;
    char buffer[BUFFER_SIZE];
    int read_len, len; 
    while(true)
    {
        len = sizeof(socket_client);
        read_len = recvfrom(sockfd, (char *)buffer, BUFFER_SIZE,
            MSG_WAITALL, ( struct sockaddr *) &socket_client,
            &len);
        
        bool found = false;

        //poor man's std::find
        for (size_t i = 0; i < next_client_idx; i++)
            if(!memcmp(&socket_client,&clients[i],len))   //entry exists
            {
                found = true;
                break;
            }
        
        if(!found)
        {
            printf("[new client] %d\n",next_client_idx);
            memcpy(&clients[next_client_idx], &socket_client, len);
            next_client_idx++;
        }
        
        
        buffer[read_len]='\0';  //TODO already received BUFF_SIZE?
        printf("[received] %s\n", buffer);
        for (size_t i = 0; i < next_client_idx; i++)
        {
            sendto(sockfd, (const char *)buffer, strlen(buffer),
                MSG_CONFIRM, (const struct sockaddr *) &clients[i],
                len);
        }
        
    }
    return 0;
}

bool line_equals(char *line, char *string)
{
    size_t len = 0;
    while(true)
    {
        char c1 = line[len];
        char c2 = string[len];
        if(c1 == '\0'
            || c1 == '\r'
            || c1 == '\n')
            return (c2  == '\0');    //true when both ended, false when string did not end yet
        if(c2  == '\0')
            return false;           //string ended first
        if(tolower(c1)!=tolower(c2))
            return false;           //content mismatch
        len++;
    }
}

