#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#define UNUSED(x) (void)(x)

bool line_equals(char *line, char *string);

int main(int argc, char const *argv[])
{
    int sock = 0, read_len;
    struct sockaddr_in serv_addr;
    char* buffer = NULL;
    size_t len = 0;
    long port;
    
    if(argc != 3)
    {
        printf("Invalid argument count! Call \"%s address port_number\"\n", argv[0]);
        exit(1);
    }
    port = strtol(argv[2], NULL, 10);
    if(port >= 65535)
    {
        printf("Invalid port! Correct range is 0-65535");
        exit(1);
    }

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Error: socket\n");
        return -1;
    }
   
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
       
    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0) 
    {
        printf("Error: inet_pton\n");
        return -1;
    }
   
    if(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("Error connection Failed\n");
        return -1;
    }
    while (true)
    {
        getline(&buffer, &len, stdin);
        send(sock, buffer, strlen(buffer), 0);
        read_len = read(sock, buffer, 1024);
        if(line_equals(buffer,"quit")
            || line_equals(buffer,"exit"))
            {
                printf("closing connection\n");
                shutdown(sock, SHUT_WR);
                while(read_len !=0)
                    read_len = read(sock, buffer, 1024);
                shutdown(sock, SHUT_RD);
                close(sock);
                exit(0);
            }
        printf("received \"%s\"\n",buffer );
    }
    return 0;
}

bool line_equals(char *line, char *string)
{
    size_t len = 0;
    while(true)
    {
        char c1 = line[len];
        char c2 = string[len];
        if(c1 == '\0'
            || c1 == '\r'
            || c1 == '\n')
            return (c2  == '\0');    //true when both ended, false when string did not end yet
        if(c2  == '\0')
            return false;           //string ended first
        if(tolower(c1)!=tolower(c2))
            return false;           //content mismatch
        len++;
    }
}