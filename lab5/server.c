#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#define UNUSED(x) (void)(x)

//constexpr please
#define BUFFER_SIZE 1024
bool line_equals(char *line, char *string);
int main(int argc, char const *argv[])
{
    long port;
    int socket_listen, socket_new;
    struct sockaddr_in address; //weird to see "struct sockaddr_in", i'm used to typedef'ing -> (t_)"sockaddr_in"
    int opt = 1;
    socklen_t addrlen = sizeof(address);
    char buffer[BUFFER_SIZE];
    
    if (argc != 2)
    {
        printf("Invalid argument count! Call \"%s port_number\"\n", argv[0]);
        exit(1);
    }
    port = strtol(argv[1], NULL, 10);
    if (port > 65535)
    {
        printf("Invalid port! Correct range is 0-65535\n");
        exit(1);
    }

    if ((socket_listen = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        printf("Error: socket failed\n");
        exit(1);
    }

    if (setsockopt(socket_listen, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        printf("Error: setsockopt failed\n");
        exit(1);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(socket_listen, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        printf("Error: bind failed\n");
        exit(1);
    }
    if (listen(socket_listen, 3) < 0)
    {
        printf("Error: listen\n");
        exit(1);
    }

    printf("Listening to port %ld\n", port);
    while(true)
    {
        if ((socket_new = accept(socket_listen, (struct sockaddr *)&address, &addrlen)) < 0)
        {
            printf("Error: accept\n");
            exit(1);
        }
        char * read_message;
        FILE * read_file = fdopen(socket_new,"r");
        while(true)
        {
            read_message = fgets(buffer,1024,read_file);
            
            if(feof(read_file) || ferror(read_file) || read_message == NULL
                || line_equals(buffer,"quit")
                || line_equals(buffer,"exit"))
            {
                printf("closing connection\n");
                shutdown(socket_new, SHUT_WR);
               
                while(!(feof(read_file) || ferror(read_file) || read_message == NULL))
                    read_message = fgets(buffer,1024,read_file);
                
                fclose(read_file);
                shutdown(socket_new, SHUT_RD);
                close(socket_new);
                printf("closed connection\n");
                break;
            }   

            send(socket_new, read_message, strlen(read_message), 0);
            printf("received \"%s\"", read_message);  
        }
    }
    return 0;
}

bool line_equals(char *line, char *string)
{
    size_t len = 0;
    while(true)
    {
        char c1 = line[len];
        char c2 = string[len];
        if(c1 == '\0'
            || c1 == '\r'
            || c1 == '\n')
            return (c2  == '\0');    //true when both ended, false when string did not end yet
        if(c2  == '\0')
            return false;           //string ended first
        if(tolower(c1)!=tolower(c2))
            return false;           //content mismatch
        len++;
    }
}

