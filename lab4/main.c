#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>

// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE DEFINES BEGIN
// * * * * * * * * * * * * * * * * * * * * * * * * * * *

// #define PRINT_IGNORE_VALID 1
// #define PRINT_IGNORE_SLEEP 1

// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE DEFINES END
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define ERROR_PRINT(x) {fprintf(stderr,x); exit(1);} 
#define UNUSED(x) (void)(x)

typedef struct 
{
    int threadId;
    const unsigned accountId;
    const int change;
    const unsigned repeats;
} OperationSingle/*Account*/;


typedef struct 
{
    int threadId;
    unsigned accountIdSource;
    unsigned accountIdDestination;
    int change;
    const unsigned repeats;
} OperationDual/*Account*/;

typedef struct 
{
    int value;
    int expectedValue;
    bool isLocked;
    pthread_cond_t condVar;
} Account;

pthread_mutex_t mutex;

void *operationSingleThread(void *OperationSingle);
inline void printOperationSinglePrefix(OperationSingle *op, int iteration);
void *operationDualThread(void *operation);
inline void printOperationDualPrefix(OperationDual *op, int iteration);

// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE VALUES BEGIN
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
OperationSingle gOperationsSingle[] = 
{
    {.accountId=0, .change=-10, .repeats=100},
    {.accountId=0, .change=-50, .repeats=50},
    {.accountId=0, .change=-5, .repeats=150},
    {.accountId=0, .change=30, .repeats=300},
    {.accountId=0, .change=3, .repeats=500},
    {.accountId=0, .change=4, .repeats=500},
    
    {.accountId=1, .change=-20, .repeats=100},
    {.accountId=1, .change=10, .repeats=150},
    {.accountId=1, .change=1, .repeats=200},
    {.accountId=1, .change=2, .repeats=150},

    {.accountId=2, .change=-3, .repeats=200},
    {.accountId=2, .change=2, .repeats=300},
    {.accountId=2, .change=-1, .repeats=200},
    {.accountId=2, .change=1, .repeats=200},
};

OperationDual gOperationsDual[] = 
{
    {.accountIdSource=0, .accountIdDestination=0, .change=99, .repeats=100},  //do nothing
    {.accountIdSource=0, .accountIdDestination=1, .change=11, .repeats=20},
    {.accountIdSource=1, .accountIdDestination=0, .change=-12, .repeats=20},
    {.accountIdSource=1, .accountIdDestination=0, .change=2, .repeats=20},
    {.accountIdSource=2, .accountIdDestination=1, .change=2, .repeats=20},
    {.accountIdSource=0, .accountIdDestination=2, .change=12, .repeats=100},
    {.accountIdSource=2, .accountIdDestination=0, .change=10, .repeats=100},
    {.accountIdSource=0, .accountIdDestination=1, .change=3, .repeats=100},
    {.accountIdSource=1, .accountIdDestination=2, .change=3, .repeats=100},
};

Account gAccounts[] = 
{
    {.value=77},
    {.value=55},
    {.value=3},
};
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE VALUES END
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define ACCOUNT_COUNT (sizeof(gAccounts)/sizeof(*gAccounts))
#define OPERATION_SINGLE_COUNT (sizeof(gOperationsSingle)/sizeof(*gOperationsSingle))
#define OPERATION_DUAL_COUNT (sizeof(gOperationsDual)/sizeof(*gOperationsDual))
#define THREAD_COUNT (OPERATION_SINGLE_COUNT + OPERATION_DUAL_COUNT)
pthread_t gOperationThread[THREAD_COUNT];

int main(int argc, char *argv[])
{
    //init
    UNUSED(argc);
    UNUSED(argv);
    srand(time(0));
    
    //init accounts
    for (size_t i = 0; i < ACCOUNT_COUNT; i++)
    {
        Account* ac = &gAccounts[i];
        ac->isLocked = false;
        ac->expectedValue = ac->value;

        printf("Account %ld contains %d bolívar soberano\n", i, ac->value);
        if (pthread_cond_init(&ac->condVar, NULL) != 0)
            ERROR_PRINT("\ncondition variable init failed\n");
    }

    //init mux
    if (pthread_mutex_init(&mutex, NULL) != 0)
        ERROR_PRINT("\nmutex init failed\n");
    printf("\n");
    
    //init operations, calculate expected end result
    for (size_t i = 0; i < OPERATION_SINGLE_COUNT; i++)
    {
        OperationSingle * op = &gOperationsSingle[i];
        op->threadId=i+1;
        gAccounts[op->accountId].expectedValue += op->change * op->repeats;
    }
    for (size_t i = 0; i < OPERATION_DUAL_COUNT; i++)
    {
        OperationDual * op = &gOperationsDual[i];
        op->threadId=OPERATION_SINGLE_COUNT+i+1;
        gAccounts[op->accountIdSource].expectedValue -= op->change * op->repeats;
        gAccounts[op->accountIdDestination].expectedValue += op->change * op->repeats;
    }
    
    printf("[account:threadId:current_iteration/all_iterations]\n");

    for (size_t i = 0; i < OPERATION_SINGLE_COUNT; i++)
        pthread_create(&gOperationThread[i],NULL,operationSingleThread,&gOperationsSingle[i]);
    for (size_t i = 0; i < OPERATION_DUAL_COUNT; i++)
        pthread_create(&gOperationThread[OPERATION_DUAL_COUNT+i],NULL,operationDualThread,&gOperationsDual[i]);
    
    //wait until all tasks are finished
    for (size_t i = 0; i < THREAD_COUNT; i++)
        pthread_join(gOperationThread[i],NULL);
    
    //destruct
    printf("\n");
    for (size_t i = 0; i < ACCOUNT_COUNT; i++)
    {
        Account *ac = &gAccounts[i];
        printf("Account %ld contains %d bolívar soberano, ", i, ac->value);
        if(ac->value == ac->expectedValue)
            printf("value ok.\n");
        else
            printf("expected value %d!\n", ac->expectedValue);
        pthread_cond_destroy(&ac->condVar);
    }
    pthread_mutex_destroy(&mutex);

    return 0;

}

void printOperationSinglePrefix(OperationSingle *op, int iteration)
{
    printf("[%d:%d:%d/%d] ",
        op->accountId, op->threadId, iteration, op->repeats);
}

void printOperationDualPrefix(OperationDual *op, int iteration)
{
    printf("[%d->%d:%d:%d/%d] ",
        op->accountIdSource, op->accountIdDestination, op->threadId, iteration, op->repeats);
}

void *operationSingleThread(void *operation)
{
    OperationSingle * op = (OperationSingle*) operation;
    for (size_t i = 0; i < op->repeats; i++)
    {
        Account* ac = &gAccounts[op->accountId];

        int val;
        int val_after;

        while(true)
        {
            //lock account
            pthread_mutex_lock(&mutex); //TODO check retval
            while(ac->isLocked)
                pthread_cond_wait(&ac->condVar,&mutex);
            ac->isLocked = true;
            pthread_mutex_unlock(&mutex);

            
            // check if new value < 0
            val = ac->value;
            val_after = val + op->change;
            if(val_after>=0)
                //operation ok, continue transaction
                break;
            #ifndef PRINT_IGNORE_SLEEP
                printOperationSinglePrefix(op,i);
                printf("%d %c %d < 0 (%d), sleeping.\n",
                    val, op->change>=0?'+':'-', abs(op->change), val_after);
                fflush(stdout);
            #endif

            //negative account value, release lock
            pthread_mutex_lock(&mutex);
            ac->isLocked = false;
            pthread_cond_signal(&ac->condVar);
            pthread_mutex_unlock(&mutex);
        } 
        
        usleep(rand() % 2500 + 500);

        // check if lock works -> value did not change unexpectedly
        if (val != ac->value)
        {
            printOperationSinglePrefix(op,i);
            printf("val=%d has changed to val=%d!\n",
                val, ac->value);
            continue;
        }

        //do transaction
        ac->value = val_after;
        
        #ifndef PRINT_IGNORE_VALID
            printOperationSinglePrefix(op,i);
            printf("%d %c %d = %d\n",
                val, op->change>=0?'+':'-', abs(op->change), val_after);
        #endif

        //unlock account
        pthread_mutex_lock(&mutex);
        ac->isLocked = false;
        pthread_cond_signal(&ac->condVar);
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}

/*
 *  Transfer op.value from source to destination
 *
 *  source -= op.value
 *  destination += op.value 
 */
void *operationDualThread(void *operation)
{
    OperationDual * op = (OperationDual*) operation;

    //parse operation
    if(op->accountIdSource == op->accountIdDestination)
    {
        printOperationDualPrefix(op,0);
        printf("operation scheduled on single accont, ignoring!\n");
        return NULL;
    }
    
    if(op->change<0)
    {
        printOperationDualPrefix(op,0);
        printf("negative operation, switching operation direction [%d->%d]\n",op->accountIdDestination,op->accountIdSource);
        // std::swap(op->accountIdDestination,op->accountIdSource)
        unsigned temp = op->accountIdSource;
        op->accountIdSource = op->accountIdDestination;
        op->accountIdDestination = temp;
        op->change = -op->change;
    }

    for (size_t i = 0; i < op->repeats; i++)
    {
        Account* src = &gAccounts[op->accountIdSource];
        Account* dest = &gAccounts[op->accountIdDestination];

        int src_val;
        int dest_val;
        int val_after;

        while(true)
        {
            //lock accounts
            pthread_mutex_lock(&mutex); //TODO check retval
            while(src->isLocked || dest->isLocked)
            {
                if(src->isLocked)
                    pthread_cond_wait(&src->condVar,&mutex);
                if(dest->isLocked)
                    pthread_cond_wait(&dest->condVar,&mutex);
            }
            src->isLocked = true;
            dest->isLocked = true;
            pthread_mutex_unlock(&mutex);

            
            // check if source account after operation will be >= 0
            src_val = src->value;
            dest_val = dest->value;
            val_after = src_val - op->change;
            if(val_after>=0)
                //operation ok, continue transaction
                break;
            #ifndef PRINT_IGNORE_SLEEP
                printOperationDualPrefix(op,i);
                printf("%d - %d < 0 (%d), sleeping.\n",
                    src_val, op->change, val_after);
                fflush(stdout);
            #endif

            //negative account value, release locks
            pthread_mutex_lock(&mutex);
            src->isLocked = false;
            dest->isLocked = false;
            pthread_cond_signal(&src->condVar);
            pthread_cond_signal(&dest->condVar);
            pthread_mutex_unlock(&mutex);
        } 
        
        usleep(rand() % 2500 + 500);

        // check if lock works -> value did not change unexpectedly
        if (src_val != src->value)
        {
            printOperationDualPrefix(op,i);
            printf("source val=%d has changed to val=%d!\n",
                src_val, src->value);
            continue;
        }
        if (dest_val != dest->value)
        {
            printOperationDualPrefix(op,i);
            printf("destination val=%d has changed to val=%d!\n",
                dest_val, dest->value);
            continue;
        }

        //do transaction
        src->value -= op->change;
        dest->value += op->change;
        
        #ifndef PRINT_IGNORE_VALID
            printOperationDualPrefix(op,i);
            printf("%d - %d = %d\t\t%d + %d = %d\n",
                src_val, op->change, src->value,
                dest_val, op->change, dest->value);
        #endif

        //unlock account
        pthread_mutex_lock(&mutex);
        src->isLocked = false;
        dest->isLocked = false;
        pthread_cond_signal(&src->condVar);
        pthread_cond_signal(&dest->condVar);
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}