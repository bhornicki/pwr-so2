#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>

// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE DEFINES BEGIN
// * * * * * * * * * * * * * * * * * * * * * * * * * * *

// #define PRINT_IGNORE_VALID 1
// #define PRINT_IGNORE_SLEEP 1

// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE DEFINES END
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define ERROR_PRINT(x) {fprintf(stderr,x); exit(1);} 
#define UNUSED(x) (void)(x)

typedef struct 
{
    int thread_id;
    const unsigned account_id;
    const int change;
    const unsigned repeats;
} Operation;

typedef struct 
{
    int value;
    pthread_mutex_t mutex;
    pthread_cond_t condVar;
} Account;

void *thread(void *operation);
inline void printOperationPrefix(Operation *op, int iteration);

// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE VALUES BEGIN
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
Operation gOperations[] = 
{
    {.account_id=0, .change=-10, .repeats=100},
    {.account_id=0, .change=-50, .repeats=50},
    {.account_id=0, .change=-5, .repeats=150},
    {.account_id=0, .change=30, .repeats=300},
    {.account_id=0, .change=3, .repeats=500},
    {.account_id=0, .change=4, .repeats=500},
    
    {.account_id=1, .change=-20, .repeats=100},
    {.account_id=1, .change=10, .repeats=150},
    {.account_id=1, .change=1, .repeats=200},
    {.account_id=1, .change=2, .repeats=150},

    {.account_id=2, .change=-3, .repeats=200},
    {.account_id=2, .change=2, .repeats=300},
    {.account_id=2, .change=-1, .repeats=200},
    {.account_id=2, .change=1, .repeats=200},
};

Account gAccounts[] = 
{
    {.value=77},
    {.value=55},
    {.value=3},
};
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
//      USER CHANGEABLE VALUES END
// * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define ACCOUNT_COUNT (sizeof(gAccounts)/sizeof(*gAccounts))
#define OPERATION_COUNT (sizeof(gOperations)/sizeof(*gOperations))
pthread_t gOperationThread[OPERATION_COUNT];

int main(int argc, char *argv[])
{
    //init
    UNUSED(argc);
    UNUSED(argv);
    srand(time(0));
    
    for (size_t i = 0; i < ACCOUNT_COUNT; i++)
    {
        Account* ac = &gAccounts[i];
        printf("Account %ld contains %d bolívar soberano\n", i, ac->value);
        if (pthread_mutex_init(&ac->mutex, NULL) != 0)
            ERROR_PRINT("\nmutex init failed\n");
        if (pthread_cond_init(&ac->condVar, NULL) != 0)
            ERROR_PRINT("\ncondition variable init failed\n");
    }
    printf("\n");
    
    for (size_t i = 0; i < OPERATION_COUNT; i++)
        gOperations[i].thread_id=i+1;
    
    
    printf("[account:thread_id:current_iteration/all_iterations]\n");

    for (size_t i = 0; i < OPERATION_COUNT; i++)
        pthread_create(&gOperationThread[i],NULL,thread,&gOperations[i]);
    
    //wait until all tasks are finished
    for (size_t i = 0; i < OPERATION_COUNT; i++)
        pthread_join(gOperationThread[i],NULL);
    
    //destruct
    printf("\n");
    for (size_t i = 0; i < ACCOUNT_COUNT; i++)
    {
        Account *ac = &gAccounts[i];
        printf("Account %ld contains %d bolívar soberano\n", i, ac->value);
        pthread_mutex_destroy(&ac->mutex);
        pthread_cond_destroy(&ac->condVar);
    }

    return 0;

}

void printOperationPrefix(Operation *op, int iteration)
{
    printf("[%d:%d:%d/%d] ",
        op->account_id, op->thread_id, iteration, op->repeats);
            
}

void *thread(void *operation)
{
    Operation * op = (Operation*) operation;
    for (size_t i = 0; i < op->repeats; i++)
    {
        Account* ac = &gAccounts[op->account_id];

        pthread_mutex_lock(&ac->mutex); //TODO check retval
        
        
        int val;
        int val_after;

        // sleep if new value < 0
        while(true)
        {
            val = ac->value;
            val_after = val + op->change;
            if(val_after>=0)
                break;
            #ifndef PRINT_IGNORE_SLEEP
                printOperationPrefix(op,i);
                printf("%d %c %d < 0 (%d), sleeping.\n",
                    val, op->change>=0?'+':'-', abs(op->change), val_after);
                fflush(stdout);
            #endif
            pthread_cond_wait(&ac->condVar,&ac->mutex);
        } 
        
        usleep(rand() % 2500 + 500);

        // check if lock works -> value did not change unexpectedly
        if (val != ac->value)
        {
            printOperationPrefix(op,i);
            printf("Illegal operation %c%d: val=%d has changed to val=%d!\n",
                op->change>=0?'+':'-', abs(op->change),
                val, op->change);
            continue;
        }

        ac->value = val_after;
        
        #ifndef PRINT_IGNORE_VALID
            printOperationPrefix(op,i);
            printf("%d %c %d = %d\n",
                val, op->change>=0?'+':'-', abs(op->change), val_after);
        #endif

        pthread_cond_signal(&ac->condVar);
        pthread_mutex_unlock(&ac->mutex);
    }

    return NULL;
}