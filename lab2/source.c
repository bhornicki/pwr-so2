#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <math.h>

#define PIPE_WR 1
#define PIPE_RD 0

int main(int argc, char *argv[])
{
    /*
	 * Parse arguments
	 */

    if (argc == 1)
    {
        fprintf(stderr, "Use ./zad2 function1 function1_argument1 function1_argumentN + ");
        fprintf(stderr, "function2 function2_argument1 function2_argumentN + ");
        fprintf(stderr, "functionN functionN_argument1 functionN_argumentN\n");
        return 1;
    }

    /*
     * 1. Count functions
     */
    size_t commandCount = 1; //last one doesn't end with '+', also we have checked if we have >1 arg
    uint8_t previousSpecial = 0;
    for (size_t i = 0; i < argc; i++)
        if (strcmp((char *)argv[i], "+\0") == 0)
        {
            if (previousSpecial)
            {
                fprintf(stderr, "Empty command (\" + + \") was found! Aborting.\n");
                return 1;
            }
            previousSpecial = 1;
            commandCount++;
            argv[i] = NULL;
        }
        else
            previousSpecial = 0;

    // fprintf(stderr, "[info] found %zd commands\n", commandCount);

    /*
     * 2. Prepare pipes between childs
     */
    int *pipes = NULL;

    if (commandCount > 1)
    {
        pipes = malloc(sizeof(int) * 2 * (commandCount - 1));
        if (!pipes)
        {
            fprintf(stderr, "Failed to allocate memory for pipes! Aborting.\n");
            return 1;
        }
        for (size_t i = 0; i < commandCount - 1; i++)
        {
            if (pipe(pipes + i * 2) < 0)
            {
                fprintf(stderr, "Failed to create %zd pipe! Aborting.", i);
                return 1;
            }
        }
    }

    /*
     * 3. Create threads
     */
    int pid;

    size_t commandOffset = 0;
    for (size_t i = 0; i < commandCount; i++)
    {
        if (commandOffset <= 0)
            commandOffset = 1;
        else
            while (++commandOffset < argc)
                if (argv[commandOffset] == NULL)
                {
                    commandOffset++;
                    break;
                }

        //create process
        pid = fork();

        if (pid)
        {
            //parent
            continue;
        }

        //child
        //variant 1: only one command
        //variant 2: first command (stdout to pipe out)
        //variant 3: middle command (pipe in to exec, stdout to pipe out)
        //variant 4: last command (pipe out to parent,)
        if (i != 0) //not first -> replace stdin
        {
            close(STDIN_FILENO);
            dup2(*(pipes + 2 * (i - 1) + PIPE_RD), STDIN_FILENO);
        }

        if (i != commandCount - 1) //not last -> replace stdout
        {
            close(STDOUT_FILENO);
            dup2(*(pipes + 2 * i + PIPE_WR), STDOUT_FILENO);
        }

        //execute command
        // fprintf(stderr,"[info] command %ld (offset %ld)\n", i, commandOffset);
        int ret = execv(argv[commandOffset], &argv[commandOffset]);
        // fprintf(stderr,"[info]   ended with code %d\n", ret);
        exit(1);
    }

    /*
     * 4. wait for the last thread 
     */
    waitpid(pid, NULL, 0);

    /*
     * 5. free pipes array memory
     */
    free(pipes);

    /*
     *  6. The end
     */
    return 0;
}